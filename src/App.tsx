import React from "react";
import { Container } from "react-bootstrap";
import { AddToMusicListForm } from "./components/AddToMusicListForm/AddToMusicListForm";

import { NavBarApp } from "./components/NavBarApp/NavBarApp";
import { useAppSelector } from "./hooks/useAppSelector";
import { generateIntl } from "./intl/intl";
import { translations } from "./lang";
import { RawIntlProvider } from "react-intl";
import { MusicList } from "./components/MusicList/MusicList";
export const App: React.FC<unknown> = () => {
  const locale = useAppSelector((state) => state.locale.locale);
  const intl = generateIntl({ locale, messages: translations[locale] });

  return (
    <RawIntlProvider value={intl}>
      <NavBarApp />
      <Container className="m-auto main-container" as="main">
        <h1 className="text-center mb-3">Music List</h1>
        <AddToMusicListForm />
        <MusicList />
      </Container>
    </RawIntlProvider>
  );
};

export default App;
