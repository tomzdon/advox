export interface IMusicList {
  id: number;
  date: number;
  title: string;
  bestOfTheBest: boolean;
}
