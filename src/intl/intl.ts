import { createIntl, createIntlCache } from "react-intl";

const cache = createIntlCache();
export const generateIntl = (props: Parameters<typeof createIntl>[0]) => {
  return createIntl(props, cache);
};

