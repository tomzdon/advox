import messagesEN from './en.json';
import messagesPL from './pl.json';

interface Translation {
    [key: string]: string;
}
interface Translations {
    [locale: string]: Translation;
}

export const translations: Translations = {
    en: messagesEN,
    pl: messagesPL,
};
