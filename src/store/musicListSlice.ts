import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IMusicList } from "../interfaces/musicList.interface";

interface IMusicListState {
  list: IMusicList[];
}

const initialState: IMusicListState = {
  list: [],
};

let previousId = 0;

const musicListSlice = createSlice({
  name: "musicList",
  initialState,
  reducers: {
    addToMusicList(state, action: PayloadAction<IMusicList["title"]>) {
      const id = previousId + 1;
      const musicList: IMusicList = {
        id: id,
        date: Date.now(),
        title: action.payload,
        bestOfTheBest: false,
      };
      state.list.unshift(musicList);
      previousId = id;
    },

    deleteFromMusicList(state, action: PayloadAction<IMusicList["id"]>) {
      const indexOfTargetMusicList = state.list.findIndex(
        (musicList) => musicList.id === action.payload
      );

      if (indexOfTargetMusicList >= 0) {
        state.list.splice(indexOfTargetMusicList, 1);
      }
    },

    setBestOfTheBest(state, action: PayloadAction<IMusicList["id"]>) {
      const targetMusicList = state.list.find(
        (musicList) => musicList.id === action.payload
      );

      if (targetMusicList) {
        targetMusicList.bestOfTheBest = !targetMusicList.bestOfTheBest;
      }
    },
  },
});

export const { addToMusicList, deleteFromMusicList, setBestOfTheBest } =
  musicListSlice.actions;

export default musicListSlice.reducer;
