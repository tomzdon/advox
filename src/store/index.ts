import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'

import musicListReducer from './musicListSlice';
import localeReducer from './localeSlice';
import storage from 'redux-persist/lib/storage';
import {setupListeners} from "@reduxjs/toolkit/query";

const persistConfig = {
  key: 'root',
  storage: storage,
}
export const rootReducers = combineReducers({
  musicList: musicListReducer,
  locale: localeReducer
})
const persistedReducer = persistReducer(persistConfig,rootReducers)

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
      }),
})
setupListeners(store.dispatch)

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
