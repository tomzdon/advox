import { createSlice, PayloadAction } from "@reduxjs/toolkit";
interface ILocaleState {
  locale: string;
  display: string;
}

const initialState: ILocaleState = {
  locale: "en",
  display: "flex",
};

const localeSlice = createSlice({
  name: "locale",
  initialState,
  reducers: {
    changeLocale(state, action: PayloadAction<ILocaleState["locale"]>) {
      state.locale = action.payload;
    },
    changeDisplay(state, action: PayloadAction<ILocaleState["display"]>) {
      state.display = action.payload;
    },
  },
});

export const { changeLocale, changeDisplay } = localeSlice.actions;

export default localeSlice.reducer;
