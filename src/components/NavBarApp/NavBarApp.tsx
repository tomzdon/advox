import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import React, { useCallback } from "react";
import { useIntl } from "react-intl";
import {changeDisplay, changeLocale} from "../../store/localeSlice";
import { useAppDispatch } from "../../hooks/useAppDispatch";

export const NavBarApp: React.FC<unknown> = () => {
  const intl = useIntl();
  const dispatch = useAppDispatch();

  const handleChangeLanguageEn = useCallback(() => {
    dispatch(changeLocale("en"));
  }, [dispatch]);
  const handleChangeLanguagePl = useCallback(() => {
    dispatch(changeLocale("pl"));
  }, [dispatch]);
  const handleChangeLanguageFlex = useCallback(() => {
    dispatch(changeDisplay("flex"));
  }, [dispatch]);
  const handleChangeLanguageGrid = useCallback(() => {
    dispatch(changeDisplay("grid"));
  }, [dispatch]);

  return (
    <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark" className="mb-3">
      <Container>
        <Nav className="me-auto">
          <NavDropdown
            title={intl.formatMessage({
              id: "list.language",
              defaultMessage: "List languages",
            })}
            id="basic-nav-dropdown"
          >
            <NavDropdown.Item onClick={handleChangeLanguageEn} href="#">
              {intl.formatMessage({
                id: "list.language.en",
                defaultMessage: "English",
              })}
            </NavDropdown.Item>
            <NavDropdown.Item onClick={handleChangeLanguagePl} href="#">
              {intl.formatMessage({
                id: "list.language.pl",
                defaultMessage: "Polish",
              })}
            </NavDropdown.Item>
          </NavDropdown>
          <NavDropdown
              title={intl.formatMessage({
                id: "display.change",
                defaultMessage: "Change Display",
              })}
              id="basic-nav-dropdown-display"
          >
            <NavDropdown.Item onClick={handleChangeLanguageFlex} href="#">
              {intl.formatMessage({
                id: "display.flex",
                defaultMessage: "Flex",
              })}
            </NavDropdown.Item>
            <NavDropdown.Item onClick={handleChangeLanguageGrid} href="#">
              {intl.formatMessage({
                id: "display.grid",
                defaultMessage: "Grid",
              })}
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Container>
    </Navbar>
  );
};
