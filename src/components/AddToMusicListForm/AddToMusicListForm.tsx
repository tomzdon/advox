import React, { FormEvent, useState } from "react";
import { Form, Button, FormProps, Col, Row } from "react-bootstrap";
import { Plus } from "react-bootstrap-icons";

import { useAppDispatch } from "../../hooks/useAppDispatch";
import { IMusicList } from "../../interfaces/musicList.interface";
import { addToMusicList } from "../../store/musicListSlice";
import { useIntl } from "react-intl";

export const AddToMusicListForm: React.FC<FormProps> = ({
  className,
  ...props
}) => {
  const dispatch = useAppDispatch();
  const intl = useIntl();
  const [title, setTitle] = useState<IMusicList["title"]>("");

  const submitHandler = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const clearedTitle = title.trim().replace(/\s{2,}/g, " ");

    if (clearedTitle.length > 0) {
      dispatch(addToMusicList(clearedTitle));
      setTitle("");
    }
  };

  return (
    <Form {...props} onSubmit={(e) => submitHandler(e)}>
      <Row>
        <Col xs={10}>
          <Form.Group
            controlId="addMusicListForm.TitleInput"
            className="flex-grow-1"
          >
            <Form.Label visuallyHidden>
              {intl.formatMessage({
                id: "title.album",
                defaultMessage: "Title album",
              })}
            </Form.Label>
            <Form.Control
              type="text"
              placeholder={intl.formatMessage({id: 'placeholder.title', defaultMessage:'Music title'})}
              autoFocus
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </Form.Group>
        </Col>
        <Col xs={2} className="d-flex justify-content-end">
          <Button variant="primary" type="submit" disabled={!title.trim()}>
            <Plus />
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
