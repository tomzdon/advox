import React, { useCallback } from "react";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { Button, Col, Row } from "react-bootstrap";
import { Trash, Star, StarFill } from "react-bootstrap-icons";
import { IMusicList } from "../../interfaces/musicList.interface";
import {
  deleteFromMusicList,
  setBestOfTheBest,
} from "../../store/musicListSlice";

interface MusicListItemProps {
  list: IMusicList;
}

export const MusicListItem: React.FC<MusicListItemProps> = ({ list }) => {
  const dispatch = useAppDispatch();

  const handleDelete = useCallback(() => {
    dispatch(deleteFromMusicList(list.id));
  }, [dispatch, list.id]);

  const handleSetBestOfTheBest = useCallback(() => {
    dispatch(setBestOfTheBest(list.id));
  }, [dispatch, list.id]);

  return (
    <Row className="mb-3 border rounded align-items-center justify-content-center py-1">
      <Col xs={1}>
        <span>{list.id}</span>
      </Col>
      <Col xs={8}>
        <span>{list.title}</span>
      </Col>
      <Col xs={3} className="d-flex justify-content-end">
        <Button
          variant="danger"
          aria-label="delete"
          onClick={handleDelete}
          className="d-flex align-items-center"
          size="sm"
        >
          <Trash />
        </Button>
        <div className="d-flex align-items-center ms-1 ">
          {list.bestOfTheBest ? (
            <StarFill fill="yellow" onClick={handleSetBestOfTheBest} />
          ) : (
            <Star onClick={handleSetBestOfTheBest} />
          )}
        </div>
      </Col>
    </Row>
  );
};
