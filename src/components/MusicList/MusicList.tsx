import { useAppSelector } from "../../hooks/useAppSelector";
import { Button, Col, Container, Row } from "react-bootstrap";
import { MusicListItem } from "../MusicListItem/MusicListItem";
import { ArrowUp, ArrowDown } from "react-bootstrap-icons";
import { useIntl } from "react-intl";
import React, { useCallback, useEffect, useState } from "react";
import { IMusicList } from "../../interfaces/musicList.interface";

export const MusicList = () => {
  const [sortID, setSortID] = useState<boolean>(false);
  const [sortDate, setSortDate] = useState<boolean>(false);
  const [sortName, setSortName] = useState<boolean>(false);
  const [listMusic, setListMusic] = useState<IMusicList[]>([]);
  const listMusicFromStore = useAppSelector((state) => state.musicList.list);
  const display = useAppSelector((state) => state.locale.display);
  const intl = useIntl();

  useEffect(() => {
    setListMusic([...listMusicFromStore]);
  }, [listMusicFromStore]);

  const handleSortId = useCallback(() => {
    setSortID((prevState) => !prevState);
    setListMusic(
      !sortID
        ? listMusic.sort((a, b) => a.id - b.id)
        : listMusic.sort((a, b) => b.id - a.id)
    );
  }, [listMusic, sortID]);

  const handleSortDate = useCallback(() => {
    setSortDate((prevState) => !prevState);
    setListMusic(
      !sortDate
        ? listMusic.sort((a, b) => a.date - b.date)
        : listMusic.sort((a, b) => b.date - a.date)
    );
  }, [listMusic, sortDate]);

  const handleSortName = useCallback(() => {
    setSortName((prevState) => !prevState);
    setListMusic(
      !sortName
        ? listMusic.sort((a, b) => a.title.localeCompare(b.title))
        : listMusic.sort((a, b) => b.title.localeCompare(a.title))
    );
  }, [listMusic, sortName]);

  return (
    <Container className="mt-3">
      <Row className="border rounded mb-3 py-1 align-items-center">
        <Col
          xs={12}
          lg={2}
          className="border-lg-end d-flex justify-content-between align-items-center"
        >
          <span>ID</span>
          <Button
            variant="secondary"
            size="sm"
            onClick={handleSortId}
            className="d-flex"
          >
            {sortID ? <ArrowUp /> : <ArrowDown />}
          </Button>
        </Col>
        <Col
          xs={12}
          lg={8}
          className="border-lg-end  mt-1 mt-lg-0 d-flex justify-content-between align-items-center"
        >
          <span>
            {intl.formatMessage({
              id: "sort.name",
              defaultMessage: "Name",
            })}
          </span>
          <Button
            variant="secondary"
            size="sm"
            onClick={handleSortName}
            className="d-flex"
          >
            {sortName ? <ArrowUp /> : <ArrowDown />}
          </Button>
        </Col>
        <Col className=" mt-1 mt-lg-0 d-flex justify-content-between align-items-center">
          <span>
            {intl.formatMessage({
              id: "sort.date",
              defaultMessage: "Date",
            })}
          </span>
          <Button
            variant="secondary"
            size="sm"
            onClick={handleSortDate}
            className="d-flex"
          >
            {sortDate ? <ArrowUp /> : <ArrowDown />}
          </Button>
        </Col>
      </Row>

      {display === "flex" ? (
        listMusic.map((list) => <MusicListItem key={list.id} list={list} />)
      ) : (
        <div className="d-grid">
          <div className="row">
            {listMusic.map((list) => (
              <div className="col-6">
                <MusicListItem key={list.id} list={list} />
              </div>
            ))}
          </div>
        </div>
      )}
    </Container>
  );
};
